import Foundation
import UIKit

class Constants {
    
    static let constantForContainerFrame: CGFloat = 8
    static let constantForViewInContainer: CGFloat = 12
    
    static let normalTextSize: CGFloat = 14
    static let titleTextSize: CGFloat = 22
    static let heightShowFullButton: CGFloat = 30
    
}
