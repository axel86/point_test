import UIKit
import Foundation
import WebKit

class ArticleCell: UITableViewCell {
    
    private weak var containerView: UIView?
    private weak var title: UILabel?
    private weak var author: UILabel?
    private weak var publishedAt: UILabel?
//    private weak var imageArticle: UIImageView?
    private weak var imageArticle: WKWebView?
    private weak var contentArticle: UILabel?
    private weak var limitView: UIView?
    var showFull: UIButton?
    
    
    
    private let limitHeight: CGFloat = Constants.heightShowFullButton
    
    func setup() {
        backgroundColor = .clear
        if containerView == nil {
            let view = UIView(frame: CGRect(x: Constants.constantForContainerFrame,
                                            y: Constants.constantForContainerFrame,
                                            width: bounds.size.width - (2 * Constants.constantForContainerFrame),
                                            height: bounds.size.height - (2 * Constants.constantForContainerFrame)))
            
            view.clipsToBounds = true
            view.layer.cornerRadius = 10
            view.backgroundColor = .white
            addSubview(view)
            containerView = view
        }
        
        if title == nil {
            let label = UILabel(frame:
                CGRect(x: Constants.constantForViewInContainer,
                       y: Constants.constantForViewInContainer,
                       width: containerView!.bounds.size.width - (2 * Constants.constantForViewInContainer),
                       height: 0))
            label.layer.borderColor = UIColor.clear.cgColor
            label.layer.borderWidth = 0
            label.textColor = .black
            label.font = .boldSystemFont(ofSize: Constants.titleTextSize)
            label.numberOfLines = 0
            containerView?.addSubview(label)
            title = label
        }
        if author == nil {
            let label = UILabel(frame:
                CGRect(x: Constants.constantForViewInContainer,
                       y: title!.frame.maxY + Constants.constantForViewInContainer,
                       width: containerView!.bounds.size.width - (2 * Constants.constantForViewInContainer),
                       height: 0))
            label.layer.borderColor = UIColor.clear.cgColor
            label.layer.borderWidth = 0
            label.backgroundColor = .white
            label.textColor = .black
            label.font = .systemFont(ofSize: Constants.normalTextSize)
            containerView?.addSubview(label)
            author = label
        }
        if publishedAt == nil {
            let label = UILabel(frame:
                CGRect(x: Constants.constantForViewInContainer,
                       y: author!.frame.maxY + (Constants.constantForViewInContainer),
                       width: containerView!.bounds.size.width - (2 * Constants.constantForViewInContainer),
                       height: 0))
            label.layer.borderColor = UIColor.clear.cgColor
            label.layer.borderWidth = 0
            label.backgroundColor = .white
            label.textColor = .black
            label.font = .systemFont(ofSize: Constants.normalTextSize)
            containerView?.addSubview(label)
            publishedAt = label
        }
        
//        if imageArticle == nil {
//            let view = UIImageView(frame:
//                CGRect(x: Constants.constantForViewInContainer,
//                       y: publishedAt!.frame.maxY + (Constants.constantForViewInContainer),
//                       width: containerView!.bounds.size.width - (Constants.constantForViewInContainer * 2),
//                       height: containerView!.bounds.size.width - (Constants.constantForViewInContainer * 2)))
//            view.clipsToBounds = true
//            view.layer.borderColor = UIColor.clear.cgColor
//            view.layer.borderWidth = 0
//            view.layer.cornerRadius = 5
//            view.backgroundColor = .gray
//            view.contentMode = .scaleAspectFit
//            containerView?.addSubview(view)
//            imageArticle = view
//        }
        if imageArticle == nil {
            let configuration = WKWebViewConfiguration()
            let webView = WKWebView(
                frame: CGRect(x: Constants.constantForViewInContainer,
                              y: publishedAt!.frame.maxY + (Constants.constantForViewInContainer),
                              width: containerView!.bounds.size.width - (Constants.constantForViewInContainer * 2),
                              height: containerView!.bounds.size.width - (Constants.constantForViewInContainer * 2)),
                configuration: configuration)
            webView.scrollView.isScrollEnabled = false
            webView.scrollView.bounces = false
            webView.contentMode = .scaleAspectFit
            webView.clipsToBounds = true
            webView.layer.cornerRadius = 5
            webView.backgroundColor = .gray
            containerView?.addSubview(webView)
            imageArticle = webView
        }
        
        if contentArticle == nil {
            let label = UILabel(frame:
                CGRect(x: Constants.constantForViewInContainer,
                       y: imageArticle!.frame.maxY + Constants.constantForViewInContainer, 
                       width: containerView!.bounds.size.width - (2 * Constants.constantForViewInContainer),
                       height: 0))
            label.layer.borderColor = UIColor.clear.cgColor
            label.layer.borderWidth = 0
            label.numberOfLines = 0
            label.textColor = .black
            label.font = .systemFont(ofSize: Constants.normalTextSize)
            containerView?.addSubview(label)
            contentArticle = label
        }
        
        if limitView == nil {
            let view = UIView(frame: CGRect(x: 0,
                                            y: 0,
                                            width: containerView!.frame.width,
                                            height: limitHeight))
            view.backgroundColor = .white
            containerView?.addSubview(view)
            limitView = view
        }
        
        if showFull == nil {
            let button = UIButton(frame: CGRect())
            button.backgroundColor = .white
            button.setTitle("Показать полностью", for: .normal)
            button.titleLabel?.font = .systemFont(ofSize: Constants.normalTextSize)
            
            button.setTitleColor(.blue, for: .normal)
            button.titleLabel?.textAlignment = .left
            button.sizeToFit()
            containerView?.addSubview(button)
            showFull = button
        }
    }
    
    func updateContent(text: NSAttributedString?, titleHeight: CGFloat, authorHeight: CGFloat, publishedAtHeight: CGFloat, photoHeight: CGFloat, contentHeight: CGFloat, totalHeight: CGFloat, limited: Bool) {

        showFull?.alpha = limited ? 1 : 0
        limitView?.alpha = limited ? 1 : 0
        
        if var frame = containerView?.frame {
            frame.origin.y = Constants.constantForContainerFrame
            frame.size.height = totalHeight - Constants.constantForContainerFrame * 2
            containerView?.frame = frame
        }
        
        if let containerFrame = containerView?.frame {
            title?.frame = CGRect(x: Constants.constantForViewInContainer,
                                       y: Constants.constantForViewInContainer,
                                       width: containerFrame.width - Constants.constantForViewInContainer * 2,
                                       height: titleHeight)
        }
        
        if let titleFrame = title?.frame, let containerFrame = containerView?.frame {
            author?.frame = CGRect(x: Constants.constantForViewInContainer,
                                        y: titleFrame.maxY + Constants.constantForViewInContainer,
                                        width: containerFrame.width - Constants.constantForViewInContainer * 2,
                                        height: authorHeight)
        }
        
        if let authorFrame = author?.frame, let containerFrame = containerView?.frame {
            publishedAt?.frame = CGRect(x: Constants.constantForViewInContainer,
                                             y: authorFrame.maxY + Constants.constantForViewInContainer,
                                             width: containerFrame.width - Constants.constantForViewInContainer * 2,
                                             height: publishedAtHeight)
        }
        
        if let publishedAtFrame = publishedAt?.frame, let containerFrame = containerView?.frame {
            imageArticle?.frame = CGRect(x: Constants.constantForViewInContainer,
                                         y: publishedAtFrame.maxY + Constants.constantForViewInContainer,
                                         width: containerFrame.width - Constants.constantForViewInContainer * 2,
                                         height: photoHeight)
        }
        
        if let imageFrame = imageArticle?.frame, let containerFrame = containerView?.frame {
            contentArticle?.frame = CGRect(x: Constants.constantForViewInContainer,
                                           y: imageFrame.maxY + Constants.constantForViewInContainer,
                                           width: containerFrame.width - Constants.constantForViewInContainer * 2,
                                           height: contentHeight)
        }
        
        if var frame = limitView?.frame, let containerFrame = containerView?.frame {
            frame.origin.y = containerFrame.maxY - limitHeight - 10
            frame.size.height = containerFrame.maxY - frame.minY
            limitView?.frame = frame
        }
        
        if var buttonFrame = showFull?.frame, let limitFrame = limitView?.frame {
            let newHeight: CGFloat = Constants.heightShowFullButton
            buttonFrame.origin.x = Constants.constantForViewInContainer
            buttonFrame.size.height = newHeight
            buttonFrame.origin.y = limitFrame.origin.y
            showFull?.frame = buttonFrame
        }
        
        contentArticle?.attributedText = text
    }
    
}

extension ArticleCell: BaseTableViewCell {

    func configure(for object: Any?) {
        guard let articles = object as? ArticlePresentation,
            let strImage = articles.urlToImage,
            let url = URL(string: strImage) else { return }
        setup()
        
        title?.attributedText = articles.title
        author?.attributedText = articles.author
        publishedAt?.attributedText = articles.publishedAt
        
        imageArticle?.load(URLRequest(url: url))
        
        updateContent(text: articles.description,
                      titleHeight: articles.titleHeight,
                      authorHeight: articles.authorHeight,
                      publishedAtHeight: articles.publishedAtHeight,
                      photoHeight: articles.resultPhotoHeight,
                      contentHeight: articles.descriptionHeight,
                      totalHeight: articles.isCompact ? articles.compactHeight() : articles.totalHeight,
                      limited: articles.isCompact)
        
    }
    
}
