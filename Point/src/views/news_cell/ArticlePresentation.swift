import UIKit

struct ArticlePresentation {
    let totalHeight: CGFloat
    let titleHeight: CGFloat
    let authorHeight: CGFloat
    let publishedAtHeight: CGFloat
    let resultPhotoHeight: CGFloat
    let descriptionHeight: CGFloat
    
    let heightWithoutContent: CGFloat
    
    let urlToImage: String?
    let title: NSAttributedString?
    let author: NSAttributedString
    let publishedAt: NSAttributedString
    let description: NSAttributedString?
    
    var isCompact = false

    private let compactTextHeight: CGFloat = 128
    private let compactTextLimit: CGFloat = 173
    private static let maxWidth = UIScreen.main.bounds.size.width - (Constants.constantForContainerFrame * 2) - (Constants.constantForViewInContainer * 2)
    private static let photoHeight = ArticlePresentation.maxWidth
    private let heightConstantInContainerView: CGFloat = Constants.constantForViewInContainer * 6
    
    init(with article: Article) {
        func prepareAuthor() -> String {
            if let author = article.author {
                return author
            } else if let name = article.source.name {
                return name
            } else {
                return ""
            }
        }
        
        func prepareDate() -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            guard let date = dateFormatter.date(from: article.publishedAt) else { return "" }
            
            let localFormatter = DateFormatter()
            localFormatter.dateFormat = "d MMMM yyyy HH:mm"
            localFormatter.timeZone = TimeZone.current
            return localFormatter.string(from: date)
        }
        
        if let text = article.title, text.count > 0 {
            let (attributedString, size) = NSAttributedString.getSizeFor(text: text, for: Constants.titleTextSize, type: .title)
            self.title = attributedString
            titleHeight = size.height
        } else {
            self.title = nil
            titleHeight = 0
        }
        
        if let text = article.description, text.count > 0 {
            let (attributedString, size) = NSAttributedString.getSizeFor(text: text, for: Constants.normalTextSize, type: .normal)
    
            self.description = attributedString
            if size.height >= compactTextLimit {
                isCompact = true
            }
            descriptionHeight = size.height
        } else {
            self.description = nil
            descriptionHeight = 0
        }
        
        if let urlToImage = article.urlToImage {
            self.urlToImage = urlToImage
            resultPhotoHeight = ArticlePresentation.photoHeight
        } else {
            self.urlToImage = nil
            resultPhotoHeight = 0
        }
        
        let authorSize: CGSize
        (author, authorSize) = NSAttributedString.getSizeFor(text: prepareAuthor(), for: Constants.normalTextSize, type: .normal)
        authorHeight = authorSize.height
        
        
        let publishedAtSize: CGSize
        (publishedAt, publishedAtSize) = NSAttributedString.getSizeFor(text: prepareDate(), for: Constants.normalTextSize, type: .normal)
        publishedAtHeight = publishedAtSize.height
        
        
        heightWithoutContent = titleHeight + authorHeight + publishedAtHeight + resultPhotoHeight + heightConstantInContainerView + Constants.constantForContainerFrame * 2
        
        if isCompact {
            totalHeight = max(heightWithoutContent + compactTextLimit + Constants.heightShowFullButton, heightWithoutContent + descriptionHeight)
        } else {
            totalHeight = heightWithoutContent + descriptionHeight
        }
    }
    
    func compactHeight() -> CGFloat {
        return heightWithoutContent + compactTextLimit + Constants.heightShowFullButton
    }
    
    func getDate() -> Date {
        let localFormatter = DateFormatter()
        localFormatter.dateFormat = "d MMMM yyyy HH:mm"
        localFormatter.timeZone = TimeZone.current
        guard let date = localFormatter.date(from: publishedAt.string) else { return Date() }
        return date
    }
}
