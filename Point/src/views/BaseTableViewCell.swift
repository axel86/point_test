import Foundation
import UIKit

protocol BaseTableViewCell {
    
    static func cellIdentifier() -> String
    
    func configure(for object: Any?)
    
    static func cellClass() -> AnyClass
}

extension BaseTableViewCell where Self: UITableViewCell {
    
    static func cellClass() -> AnyClass {
        return self.classForCoder()
    }
    
    static func cellIdentifier() -> String {
        return String(describing: self)
    }
    
    func configure(for object: Any?) {
        
    }
    
}
