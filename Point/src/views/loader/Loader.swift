import Foundation
import UIKit

class LoaderView: BaseView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    
    
    func setupView() {
        
        let view = UIActivityIndicatorView(style: .whiteLarge)
        addSubview(view)
        
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        view.translatesAutoresizingMaskIntoConstraints = false
    
        NSLayoutConstraint(item: view,
                           attribute: .centerX,
                           relatedBy: .equal,
                           toItem: self,
                           attribute: .centerX,
                           multiplier: 1.0,
                           constant: 0).isActive = true
        
        NSLayoutConstraint(item: view,
                           attribute: .centerY,
                           relatedBy: .equal,
                           toItem: self,
                           attribute: .centerY,
                           multiplier: 1.0,
                           constant: 0).isActive = true
        
        view.startAnimating()
    }
    
}
