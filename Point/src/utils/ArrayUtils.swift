import Foundation

extension Array where Element == ArticlePresentation {
    
    mutating func sortByDate() {
        self.sort { one, two -> Bool in
            return one.getDate() > two.getDate()
        }
    }
    
}
