import Foundation

func localized(_ key: String) -> String {
    return NSLocalizedString(key, comment: "")
}

func currentUserLanguage() -> String {
    return (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode) as? String ?? "en"
}
