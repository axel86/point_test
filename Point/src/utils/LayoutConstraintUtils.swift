import Foundation
import UIKit

extension NSLayoutConstraint {
    
    static var allFrameAttribute: [NSLayoutConstraint.Attribute] {
        return [NSLayoutConstraint.Attribute.top,
                NSLayoutConstraint.Attribute.bottom,
                NSLayoutConstraint.Attribute.trailing,
                NSLayoutConstraint.Attribute.leading]
    }
    
}
