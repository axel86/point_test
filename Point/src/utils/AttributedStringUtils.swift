import Foundation
import UIKit

enum TextType {
    case normal
    case title
}

extension NSAttributedString {
    
    static func getSizeFor(text: String, for size: CGFloat, type: TextType) -> (resultString: NSAttributedString, size: CGSize) {
        
        let attributedString = NSMutableAttributedString(string: text)
        let range = NSMakeRange(0, attributedString.string.count)
        
        let font: UIFont
        switch type {
        case .normal:
            font = .systemFont(ofSize: size)
        case .title:
            font = .boldSystemFont(ofSize: size)
        }
        
        attributedString.addAttributes([NSAttributedString.Key.font: font,
                                        NSAttributedString.Key.kern: -0.17],
                                       range: range)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byWordWrapping
        paragraphStyle.lineSpacing = 4
        attributedString.addAttributes([NSAttributedString.Key.paragraphStyle: paragraphStyle], range: range)
        
        let resultSize = attributedString.boundingRect(with: CGSize(width: UIScreen.main.bounds.width, height: CGFloat.greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil).size
        
        return (resultString: attributedString, size: resultSize)
    }
    
}
