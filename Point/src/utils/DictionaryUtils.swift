import Foundation

extension Dictionary where Key == String, Value == Any {
    
    func add(other dictionary: [String : Any]) -> [String : Any]{
        return self.merging(dictionary, uniquingKeysWith: { (one, two) -> Any in
            return two
        })
    }
}
