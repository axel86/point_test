import UIKit

extension UIRefreshControl {
    func beginRefreshingManually() {
        beginRefreshing()
        if let scrollView = superview as? UIScrollView {
            scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y - frame.height), animated: true)
        }
    }
}
