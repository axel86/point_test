import Foundation
import UIKit

enum ErrorCode: Int {
    case unknownError = -999
}

func makeError(with message: String) -> Error {
    return NSError(domain: "", code: ErrorCode.unknownError.rawValue, userInfo: [
        NSLocalizedDescriptionKey: message
        ])
}

func makeError(with errors: Dictionary<String, Any>) -> Error {
    let message = errors.map { (_, value) -> String in
        return  "\(String(describing: value))"
        }.joined(separator: "\n")
    
    return NSError(domain: "", code: ErrorCode.unknownError.rawValue, userInfo: [
        NSLocalizedDescriptionKey: message
        ])
}

func errorFrom(response: Dictionary<String, Any>) -> Error?  {
    guard let errors = response["errors"] as? Dictionary<String, Any>, !errors.isEmpty else { return  nil }
    
    return makeError(with: errors)
}

func showError(message: String, viewController: UIViewController?) {
    let alert = UIAlertController(title: localized("Error"), message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: localized("Ok"), style: .default))
    if let viewController = viewController {
        viewController.present(alert, animated: true)
    } else {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController!.present(alert, animated: true)
    }
}
