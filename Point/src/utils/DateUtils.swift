import Foundation

extension Date {
    
    static var currentTime: String {
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-d'T'HH:mm:ssZ"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter.string(from: currentDate)
    }
    
    var dateForApi: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-d'T'HH:mm:ssZ"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter.string(from: self)
    }
    
}
