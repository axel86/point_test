import Foundation

struct Article {
    let source: Source
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String
    let content: String?
}

extension Article: Decodable {
    
    private enum ArticleCodingKeys: String, CodingKey {
        case source
        case author
        case title
        case description
        case url
        case urlToImage
        case publishedAt
        case content
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ArticleCodingKeys.self)
        
        source = try container.decode(Source.self, forKey: .source)
        author = try container.decode(String?.self, forKey: .author)
        title = try container.decode(String?.self, forKey: .title)
        description = try container.decode(String?.self, forKey: .description)
        url = try container.decode(String?.self, forKey: .url)
        urlToImage = try container.decode(String?.self, forKey: .urlToImage)
        publishedAt = try container.decode(String.self, forKey: .publishedAt)
        content = try container.decode(String?.self, forKey: .content)
    }
    
}

struct Source {
    let id: String?
    let name: String?
}

extension Source: Decodable {

    private enum SourceCodingKeys: String, CodingKey {
        case id
        case name
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: SourceCodingKeys.self)
        
        id = try container.decode(String?.self, forKey: .id)
        name = try container.decode(String?.self, forKey: .name)
    }
    
}
