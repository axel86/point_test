import Foundation

struct ArticleApiResponse {
    let status: String
    let totalResults: Int
    let articles: [Article]
}

extension ArticleApiResponse: Decodable {
    
    private enum ArticleApiResponseCodingKeys: String, CodingKey {
        case status
        case totalResults
        case articles
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ArticleApiResponseCodingKeys.self)
        
        status = try container.decode(String.self, forKey: .status)
        totalResults = try container.decode(Int.self, forKey: .totalResults)
        articles = try container.decode([Article].self, forKey: .articles)
    }
    
}
