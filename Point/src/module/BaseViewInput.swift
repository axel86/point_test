import Foundation
import UIKit

protocol BaseViewInput: class {
    associatedtype ViewType
    var rootView: ViewType { get }
    func show(error: Error)
    func showAlert(title: String?, message: String?, buttonTitle: String, callback: @escaping () -> Void)
    func showLoading()
    func hideLoading()
}

extension BaseViewInput where Self: UIViewController {
    
    var rootView: ViewType {
        return view as! ViewType
    }
    
    func show(error: Error) {
        showError(message: error.localizedDescription, viewController: self)
    }
    
    func showAlert(title: String?, message: String?, buttonTitle: String, callback: @escaping () -> Void) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(
            UIAlertAction(title: buttonTitle, style: .cancel) { _ in
                callback()
            }
        )
        
        present(alert, animated: true)
    }
    
    func showAlert(title: String?, message: String?, alertActions: [String: () -> Void]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for (buttonTitle, callback) in alertActions {
            alert.addAction(
                UIAlertAction(title: buttonTitle, style: .default)  { _ in callback() }
            )
        }
        
        present(alert, animated: true)
    }
    
    func showLoading() {
        view.showLoading()
    }
    
    func hideLoading() {
        view.hideLoading()
    }
}
