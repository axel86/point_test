import Foundation
import UIKit

class NewsScreenViewController: UIViewController {
    
    var viewOutput: NewsScreenPresenterProtocol?
    
    var refreshController = UIRefreshControl()

    
    override func loadView() {
        view = NewsScreenView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rootView.viewDidLoad()
        setupTable()
        viewOutput?.onLoad()
        refreshController.beginRefreshing()
    }
    
    func setupRefreshController() {
        if #available(iOS 10.0, *) {
            rootView.tableView.refreshControl = refreshController
        } else {
            rootView.tableView.addSubview(refreshController)
        }
        
        refreshController.addTarget(self, action: #selector(pulledToRefresh), for: .valueChanged)
    }
    
    @objc func pulledToRefresh() {
        viewOutput?.getNewsTop()
    }
    
    @objc func showFull(sender: UIButton) {
        viewOutput?.setFalseCompactArticle(with: sender.tag)
        guard let article = viewOutput?.articles[sender.tag] else { return }
        let indexPath = IndexPath(row: sender.tag, section: 0)
        if let cell = rootView.tableView.cellForRow(at: indexPath) as? ArticleCell {
            rootView.tableView.beginUpdates()
            UIView.animate(withDuration: 0.3) {
                cell.updateContent(text: article.description,
                                   titleHeight: article.titleHeight,
                                   authorHeight: article.authorHeight,
                                   publishedAtHeight: article.publishedAtHeight,
                                   photoHeight: article.resultPhotoHeight,
                                   contentHeight: article.descriptionHeight,
                                   totalHeight: article.totalHeight,
                                   limited: article.isCompact)
            }
            rootView.tableView.endUpdates()
        }
    }
    
}

extension NewsScreenViewController: NewsScreenViewInput {
    
    typealias ViewType = NewsScreenView
    
    func stopRefreshing() {
        if refreshController.isRefreshing {
            refreshController.endRefreshing()
        }
    }
    
    func reloadArticles() {
        rootView.tableView.reloadData()
        stopRefreshing()
        viewOutput?.loadingMore = false
        rootView.stopAnimationLoadingIndicator()
    }
}

extension NewsScreenViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        rootView.searchBar.resignFirstResponder()
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        let additionalOffset = -(UIApplication.shared.statusBarFrame.height)
        guard let loadingMore = viewOutput?.loadingMore, let articles = viewOutput?.articles else { return }
        if deltaOffset < additionalOffset && !loadingMore && articles.count > 0 {
            viewOutput?.loadingMore = true
            rootView.startAnimatingLoadingIndicator()
            viewOutput?.getNewsFooter()
        }
    }
    
    func setupTable() {
        rootView.tableView.register(ArticleCell.self)
        
        rootView.tableView.delegate = self
        rootView.tableView.dataSource = self
        
        setupRefreshController()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewOutput?.articles.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let description = viewOutput?.getDescriptionFor(indexPath: indexPath) else { return UITableViewCell() }
        
        if let cell = tableView.configureCell(with: description, for: indexPath) as? ArticleCell {
            cell.showFull?.tag = indexPath.row
            cell.showFull?.addTarget(self, action: #selector(showFull), for: .touchUpInside)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let article = viewOutput?.articles[indexPath.row] {
            return article.isCompact ? article.compactHeight() : article.totalHeight
        }
        return UITableView.automaticDimension
    }
    
}

