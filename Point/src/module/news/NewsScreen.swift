import Foundation

protocol NewsScreenModuleInput: BaseModuleInput {
}

protocol NewsScreenModuleOutput: BaseModuleOutput {
}

protocol NewsScreenViewInput: BaseViewInput {
    func reloadArticles()
    func stopRefreshing()
}

protocol NewsScreenViewOutput: BaseViewOutput {
    var articles: [ArticlePresentation] { get }
    var fromDate: Date { get }
    var toDate: Date { get }
    var loadingMore: Bool { get set }
    func onLoad()
    func setFalseCompactArticle(with tag: Int)
    func getDescriptionFor(indexPath: IndexPath) -> TableViewCellDescription?
    
    func getNewsTop()
    func getNewsFooter()
}

protocol NewsScreenPresenterProtocol: NewsScreenViewOutput, NewsScreenModuleInput {
}
