import Foundation

class NewsScreenViewInitializer {
    
    class func newsViewController(moduleOutput: NewsScreenModuleOutput) -> NewsScreenViewController {
        let vc = NewsScreenViewController()
        
        let presenter = NewsScreenPresenter<NewsScreenViewController>()
        presenter.moduleOutput = moduleOutput
        presenter.viewInput = vc
        
        vc.viewOutput = presenter
        
        return vc
    }
}
