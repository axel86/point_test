import Foundation
import UIKit

class NewsScreenView: BaseScreenView {
    
    let tableView = UITableView()
    let topBar = UIView()
    let headerView = UIView()
    let searchBar = UISearchBar()
    let loadingIndicator = UIActivityIndicatorView()
    let footerView = UIView()
    
    private let heightForViewInTable: CGFloat = 50
    
    func viewDidLoad() {
        
        setupTopBar()
        setupTableView()
        setupHeader()
        setupFooter()
        setupTableForSmoothScrool()
    }
    
    private func setupTopBar() {
        let titleLabelView = UILabel()
        topBar.addSubview(titleLabelView)
        
        titleLabelView.backgroundColor = .clear
        titleLabelView.numberOfLines = 1
        titleLabelView.textColor = .black
        
        let attributedStringTitle: NSAttributedString
        let size: CGSize
        (attributedStringTitle, size) = NSAttributedString.getSizeFor(text: "NEWS", for: Constants.titleTextSize, type: .title)
        
        NSLayoutConstraint(item: titleLabelView,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .height,
                           multiplier: 1.0,
                           constant: size.height).isActive = true
        titleLabelView.attributedText = attributedStringTitle
        
        
        titleLabelView.translatesAutoresizingMaskIntoConstraints = false
        titleLabelView.heightAnchor.constraint(equalToConstant: size.height).isActive = true
        titleLabelView.widthAnchor.constraint(equalToConstant: size.width).isActive = true
        titleLabelView.bottomAnchor.constraint(equalTo: topBar.bottomAnchor).isActive = true
        titleLabelView.centerXAnchor.constraint(equalTo: topBar.centerXAnchor).isActive = true
 
        
        addSubview(topBar)
        topBar.backgroundColor = .white
        topBar.layer.masksToBounds = false
        topBar.layer.shadowOffset = CGSize(width: 0, height: 2)
        topBar.layer.shadowRadius = 8;
        topBar.layer.shadowColor = UIColor.black.cgColor
        topBar.layer.shadowOpacity = 0.1;
        
        topBar.translatesAutoresizingMaskIntoConstraints = false
        topBar.topAnchor.constraint(equalTo: topAnchor).isActive = true
        topBar.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        topBar.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        NSLayoutConstraint(item: topBar,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .height,
                           multiplier: 1.0,
                           constant: UIApplication.shared.statusBarFrame.height + size.height).isActive = true
    }
    
    private func setupTableView() {
        backgroundColor = .lightGray
        
        addSubview(tableView)
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: topBar.bottomAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
    }
    
    private func setupHeader() {
        headerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: heightForViewInTable)
        tableView.tableHeaderView = headerView
        headerView.backgroundColor = .clear
    
        
        let font = UIFont.systemFont(ofSize: Constants.normalTextSize)
        let textFieldAppearance = UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self])
        textFieldAppearance.backgroundColor = UIColor(red: 0, green: 0.11, blue: 0.24, alpha: 0.06)
        textFieldAppearance.font = font
        let labelAppearance = UILabel.appearance(whenContainedInInstancesOf: [UISearchBar.self])
        labelAppearance.textColor = UIColor(red: 0.5, green: 0.55, blue: 0.6, alpha: 1)
        labelAppearance.font = font
        
        headerView.addSubview(searchBar)
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.leadingAnchor.constraint(equalTo: headerView.leadingAnchor).isActive = true
        searchBar.trailingAnchor.constraint(equalTo: headerView.trailingAnchor).isActive = true
        
    }
    
    private func setupFooter() {
        footerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: heightForViewInTable * 2)
        footerView.backgroundColor = .clear
        tableView.tableFooterView = footerView
        
        footerView.addSubview(loadingIndicator)
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = .gray
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        loadingIndicator.centerXAnchor.constraint(equalTo: footerView.centerXAnchor).isActive = true
        loadingIndicator.centerYAnchor.constraint(equalTo: footerView.centerYAnchor).isActive = true
        loadingIndicator.isHidden = true
    }

    private func setupTableForSmoothScrool() {
        tableView.allowsSelection = false
        tableView.estimatedRowHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.estimatedSectionHeaderHeight = 0
    }
    
    func startAnimatingLoadingIndicator() {
        loadingIndicator.startAnimating()
    }
    
    func stopAnimationLoadingIndicator() {
        loadingIndicator.stopAnimating()
    }
}

