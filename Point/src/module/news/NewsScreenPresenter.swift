import Foundation

class NewsScreenPresenter<ViewInput: NewsScreenViewInput> {
    
    var moduleOutput: NewsScreenModuleOutput!
    weak var viewInput: ViewInput?
    var networkManager: NetworkManager = NetworkManager()
    var articles: [ArticlePresentation] = []
    var fromDate: Date = Date()
    var toDate: Date = Date.init(timeIntervalSince1970: 0)
    var loadingMore: Bool = false
    
    func onLoad() {
        networkManager.getTopHeadlines { result, error in
            print("finished request")
            DispatchQueue.main.async {
                if error != nil {
                    self.viewInput?.stopRefreshing()
                    self.viewInput?.show(error: makeError(with: error ?? "ERROR"))
                    return
                }
                print("count after: \(self.articles.count)")
                if let articles = result {
                    articles.articles.forEach({ article in
                        let presentation = ArticlePresentation(with: article)
                        let articleDate = presentation.getDate()
                        self.toDate = max(articleDate, self.toDate)
                        self.fromDate = min(articleDate, self.fromDate)
                        self.articles.append(presentation)
                    })
                }
                print("count after: \(self.articles.count)")
                self.articles.sortByDate()
                self.viewInput?.reloadArticles()
            }
        }
    }
    
    func setFalseCompactArticle(with tag: Int) {
        articles[tag].isCompact = false
    }
}

extension NewsScreenPresenter: NewsScreenPresenterProtocol {
    
    func getNewsTop() {
        
        networkManager.getEverything(to: toDate.dateForApi) { (result, error) in
            DispatchQueue.main.async {
                if error != nil {
                    self.viewInput?.show(error: makeError(with: error ?? "ERROR"))
                    return
                }
                let currentTo = self.toDate
                print("count before: \(self.articles.count)")
                if let articles = result {
                    articles.articles.forEach({ article in
                        let presentation = ArticlePresentation(with: article)
                        let articleDate = presentation.getDate()
                        if articleDate > currentTo {
                            self.articles.insert(presentation, at: 0)
                            self.toDate = max(articleDate, self.toDate)
                        }
                    })
                }
                print("count after: \(self.articles.count)")
                self.articles.sortByDate()
                self.viewInput?.reloadArticles()
            }
        }
    }
    
    func getNewsFooter() {
        networkManager.getEverything(from: fromDate.dateForApi) { (result, error) in
            DispatchQueue.main.async {
                if error != nil {
                    self.viewInput?.show(error: makeError(with: error ?? "ERROR"))
                    return
                }
                let currentFrom = self.fromDate
                print("count before: \(self.articles.count)")
                if let articles = result {
                    articles.articles.forEach({ article in
                        let presentation = ArticlePresentation(with: article)
                        let articleDate = presentation.getDate()
                        print(articleDate, currentFrom)
                        if articleDate < currentFrom {
                            self.articles.append(presentation)
                            self.fromDate = min(articleDate, self.fromDate)
                        }
                    })
                }
                print("count after: \(self.articles.count)")
                self.articles.sortByDate()
                self.viewInput?.reloadArticles()
            }
        }
    }
    
    
    func getDescriptionFor(indexPath: IndexPath) -> TableViewCellDescription? {
        if indexPath.row < articles.count {
            let article = articles[indexPath.row]
            return TableViewCellDescription(cellType: ArticleCell.self,
                                            height: article.isCompact ? article.compactHeight() : article.totalHeight,
                                            object: article)
        }
        return nil
    }
}
