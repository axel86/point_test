import UIKit

class MainFlowCoordinator: BaseFlowProtocol {
    
    private(set) var rootViewController: UIViewController?
    
    func launchViewController() -> UIViewController? {
        guard let newsScreen = newsViewController() else { return nil }
        
        rootViewController = newsScreen
        
        return rootViewController!
    }
}

extension MainFlowCoordinator: NewsScreenModuleOutput {
    
}

extension MainFlowCoordinator {
    
    fileprivate func newsViewController() -> UIViewController? {
        return NewsScreenViewInitializer.newsViewController(moduleOutput: self)
    }
    
}
