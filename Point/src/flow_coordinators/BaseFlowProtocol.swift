import Foundation
import UIKit

protocol BaseFlowProtocol {
    
    func launchViewController() -> UIViewController?
    
    func makeRoot(viewController: UIViewController?, animated: Bool)
    var rootViewController: UIViewController? { get }
    
}

extension BaseFlowProtocol {
    
    func makeRoot(viewController: UIViewController?, animated: Bool = true) {
        AppDelegate.shared.makeRoot(viewController: viewController, animated: animated)
    }
}
