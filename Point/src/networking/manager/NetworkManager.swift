import Foundation
// TODO: почистить
struct NetworkManager {
    
    static var environment: NetworkEnvironment = .topHeadlines
    static let ArticleAPIKey = "4fd7ac8c56624757b9fa664ad6ec6c57"
    static let defaultParameters: Parameters = ["q" : "Apple", "apiKey": NetworkManager.ArticleAPIKey, "language": "en"]
    private let router = Router<ArticleApi>()
    
    func getTopHeadlines(completion: @escaping RouterCompletionRequest) {
        router.request(.topHeadlines) { data, response, error in
            self.completionHandler(data, response, error, completion: completion)
        }
    }
    
    func getEverything(to date: String, completion: @escaping RouterCompletionRequest) {
        router.request(.everythingTo(date)) { (data, response, error) in
            self.completionHandler(data, response, error, completion: completion)
        }
    }
    
    func getEverything(from date: String, completion: @escaping RouterCompletionRequest) {
        router.request(.everythingfrom(date)) { (data, response, error) in
            self.completionHandler(data, response, error, completion: completion)
        }
    }
    
    fileprivate func completionHandler(_ data: Data?,_ response: URLResponse?,_ error: Error?, completion: @escaping RouterCompletionRequest) {
        
        if error != nil {
            completion(nil, "Please check your network connection.")
        }
        
        if let response = response as? HTTPURLResponse {
            print(response.statusCode)
            let result = self.handleNetworkResponse(response)
            
            switch result {
            case .success:
                guard let responseData = data else {
                    completion(nil, NetworkResponse.noData.rawValue)
                    return
                }
                
                do {
                    let apiResponse = try JSONDecoder().decode(ArticleApiResponse.self, from: responseData)
                    completion(apiResponse, nil)
                } catch {
                    completion(nil, NetworkResponse.unableToDecode.rawValue)
                }
            case .failure(let networkFailureError):
                completion(nil, networkFailureError)
            }
        }
        
    }
    
    fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String> {
        switch response.statusCode {
        case 200...299: return .success
        case 401...500: return .failure(NetworkResponse.authentificationError.rawValue)
        case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
        case 600: return .failure(NetworkResponse.outdate.rawValue)
        default: return .failure(NetworkResponse.failed.rawValue)
        }
    }
}

enum NetworkResponse: String {
    case succes
    case authentificationError = "You need to be authentificated first."
    case badRequest = "Bad request"
    case outdate = "The url you requested is outdated."
    case failed = "Network request failed"
    case noData = "Response returned with no data to decode"
    case unableToDecode = "We cloud not decode the response"
}

enum Result<String> {
    case success
    case failure(String)
}
