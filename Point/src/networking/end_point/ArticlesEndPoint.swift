import Foundation

enum NetworkEnvironment {
    case topHeadlines
    case everything
}

public enum ArticleApi {
    case topHeadlines
    case everythingTo(String)
    case everythingfrom(String)
}
// TODO : почистить
extension ArticleApi: EndPointType {
    
    var environmentBaseURL: String {
        switch NetworkManager.environment {
        case .topHeadlines: return "https://newsapi.org/v2/"
        case .everything: return "https://newsapi.org/v2/"
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else {
            fatalError("baseURL could not be configured.")
        }
        return url
    }
    
    var path: String {
        return "everything"
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var task: HTTPTask {
        switch self {
        case .topHeadlines:
 
            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: ["from": Date.currentTime,
                                                      "pageSize": 25].add(other: NetworkManager.defaultParameters))
        case .everythingTo(let to):
            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: ["to" : to,
                                                      "from" : Date.currentTime].add(other: NetworkManager.defaultParameters))
        case .everythingfrom(let from):
            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: ["from": from,
                                                      "pageSize": 25].add(other: NetworkManager.defaultParameters))
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    
}
