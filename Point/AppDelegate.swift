import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    class var shared: AppDelegate {
        get {
            return UIApplication.shared.delegate as! AppDelegate
        }
    }

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        showMainScreen(false)
        
        return true
    }

}


extension AppDelegate {
    
    func showMainScreen(_ animated: Bool = true) {
        if window == nil {
            window = UIWindow(frame: UIScreen.main.bounds)
        }
        
        let mainScreen = MainFlowCoordinator().launchViewController()
        
        makeRoot(viewController: mainScreen, animated: animated)
        
    }
    
    func makeRoot(viewController: UIViewController?, animated: Bool = true) {
        guard let viewController = viewController else { return }
        if let strongWindow = window {
            if animated {
                strongWindow.rootViewController = viewController
                UIView.transition(
                    with: strongWindow,
                    duration: 0.3,
                    options: [.curveEaseInOut, .transitionCrossDissolve, .preferredFramesPerSecond60],
                    animations: { [weak strongWindow] in
                        strongWindow?.rootViewController = viewController
                    },
                    completion: nil
                )
            } else {
                strongWindow.rootViewController = viewController
                strongWindow.makeKeyAndVisible()
            }
        }
    }
    
}

